from django.forms import ModelForm
from todos.models import TodoList

class TodoItemForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]
