from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoItem, TodoList
from todos.forms import TodoItemForm


# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todo/list.html", context)

#feature 8
def todo_list_detail(request, id):
    list = TodoList.objects.get(id = id)
    context = {
        "list": list,
    }
    return render(request, "todo/detail.html", context)


#feature 9
def todo_list_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id = todolist.id)
    else:
        form = TodoItemForm()
        context = {"form": form}

    return render(request, "todo/create.html", context)


#feature 10
def todo_list_update(request, id):
    todoList = get_object_or_404(TodoList, id = id)
    if request.method == "POST":
       form = TodoItemForm(request.POST, instance = todoList)
       if form.is_valid():
          todoList = form.save()
          return redirect("todo_list_detail", todoList.id)
    else:
        todoList = get_object_or_404(TodoList, id = id)
        form = TodoItemForm(instance = todoList)
    return render(request, "todo/edit.html", {"form":form})
